package com.webapp.gateway.crawler.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class CrawlerPostFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(CrawlerPostFilter.class);

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		return request.getRequestURI() != null && !request.getRequestURI().contains("error");
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();

		HttpServletRequest request = ctx.getRequest();
		HttpServletResponse response = ctx.getResponse();

		log.info("{} following status sent : {}", request.getRequestURL().toString(), response.getStatus());

		return null;
	}

	private String getRequestURL(HttpServletRequest request) {
		String res = "";
		if (request.getRequestURI() != null)
			res = request.getRequestURI().replaceAll("/", "_");
		String arr[] = res.split("crawler-gateway_");
		return (arr != null && arr.length > 1) ? arr[1].toUpperCase() : "CRAWLER_GATEWAY";
	}
}