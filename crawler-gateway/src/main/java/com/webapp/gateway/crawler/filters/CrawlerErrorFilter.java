package com.webapp.gateway.crawler.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class CrawlerErrorFilter extends ZuulFilter {

	private static final String REQUEST_URI = "javax.servlet.forward.request_uri";

	private static Logger log = LoggerFactory.getLogger(CrawlerErrorFilter.class);

	@Override
	public String filterType() {
		return "error";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();

		HttpServletRequest request = ctx.getRequest();
		HttpServletResponse response = ctx.getResponse();

		String originalReqUrl=getOriginalRequestURL(request);

		log.info("{} following status sent : {}", originalReqUrl, response.getStatus());

		return null;
	}

	private String getOriginalRequestURL(HttpServletRequest request) {
		String res = "";	
		if (request.getAttribute(REQUEST_URI) != null)
			res = ((String) request.getAttribute(REQUEST_URI));
		return (res.isEmpty()) ? request.getRequestURL().toString() : res;
	}

	private String getRequestURL(String requestUrl) {
		requestUrl=requestUrl.replaceAll("/", "_");
		String arr[] = requestUrl.split("crawler-gateway_");
		return (arr != null && arr.length > 1) ? arr[1].toUpperCase() : "CRAWLER_GATEWAY";
	}
}