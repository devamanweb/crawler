package com.webapp.gateway.crawler.entry;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import com.webapp.gateway.crawler.filters.CrawlerErrorFilter;
import com.webapp.gateway.crawler.filters.CrawlerPostFilter;
import com.webapp.gateway.crawler.filters.CrawlerPreFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
@ComponentScan({ "com.webapp.gateway*" })
public class CrawlerApplication implements EnvironmentAware {

	public static void main(String[] args) {
		SpringApplication.run(CrawlerApplication.class, args);
	}

	@Bean
	public CrawlerPostFilter crawlerPostFilter() {
		return new CrawlerPostFilter();
	}

	@Bean
	public CrawlerPreFilter crawlerPreFilter() {
		return new CrawlerPreFilter();
	}

	@Bean
	public CrawlerErrorFilter crawlerErrorFilter() {
		return new CrawlerErrorFilter();
	}

	@LoadBalanced
	@Bean
	public RestTemplate loadbalancedRestTemplate() {
	     return new RestTemplate();
	}
	
	@Override
	public void setEnvironment(Environment environment) {
		Properties props = new Properties();
		try {
			props.setProperty("crawlerapp.server.address", InetAddress.getLocalHost().getHostAddress() + ".client");
			PropertiesPropertySource propertySource = new PropertiesPropertySource("myProps", props);
			if (environment instanceof StandardEnvironment) {
				((StandardEnvironment) environment).getPropertySources().addFirst(propertySource);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
