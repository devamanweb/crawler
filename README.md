The following Probject uses Microservices Architecture to return a set of links crawled from a url at a depth 


It has the following Services : 

Gateway Service

Producer Service :  Push packet into kafka

RedisService : Used to check if duplicate packet is received it will not reprocess it.

ConsumerService:  Consume the packet and run crawling logic.

Service Discovery is achieved through Consul.

The Database used is Mongo.

Packet received is pushed to kafka and a token is returned back to the user. This is a unique Identifier for the user.



Following is the CURL request for Push packet
curl --location --request POST 'localhost:8000/crawler-gateway/producer/v1/crawler/push' \
--header 'Content-Type: application/json' \
--data-raw '{
	"url" : "https://www.facebook.com/",
	"depth" : 2
}'



Following is the response.

{
    "status": true,
    "token": "CRAWL6666481308903215104"
}



Following is the CURL request for getting status of packet.

curl --location --request GET 'localhost:8000/crawler-gateway/producer/v1/crawler/status?token=CRAWL6666481308903215104'


Its Response :Processed 	 

Following is the request for getting the result

curl --location --request GET 'localhost:8000/crawler-gateway/producer/v1/crawler/result?token=CRAWL6666218584994549760'


And Following is the response

{
    "totalLinks": 156,
    "totalImages": 75,
    "pageDetails": [
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - लॉग इन या साइन अप करें",
            "pageLink": "https://www.facebook.com/",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://portal.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/?ref=pf",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT1UUMBtjfN0iu4Nh81VFha2-unGo_rSrm5j61haWyyCaSKfXX6neg-Kgw3T1O09pwSSO3cad1B909mR3FRrn7TjWVKVZDD8bYM-zHlGGyNooNIK6E099mSTktwfAL-HpQ6gU3UK_adBRSSq",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 1
                }
            ],
            "currLevel": 2
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - లాగిన్ లేదా సైన్ అప్",
            "pageLink": "https://te-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://hi-in.facebook.com/?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT1t4b-cRjDkHYG_aUQUd1w0SNTq_bDa-7X9AGBLEUlOtfIJEQ3XuHago9BbWn0xv6qy55C2-_LFDmfIL32J0PTXzHg87KIhFbgeOLiHa007MB30Yh7V4P5j3UqXYvPploqJWjoGRCLNbvXy",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "पासवर्ड भूल गए | लॉग इन नहीं कर सकते | Facebook",
            "pageLink": "https://www.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
            "imageCount": 1,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT2bZuNHR5pyTp5ZAR5abfayIbYZuaxTn2J8W8_lr-jJSgLHt5CIEdy3_RaJNo0GF1ihH4JK2pyggVEEchEP7tNPxMer9HioA4gqCq-YO_gHkTpCxZkO_rxiAlghFXkEWNWeDA4mlHQPBc3x",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/recover/initiate?lwv=301&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/login/identify/?ctx=recover&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook – log in or sign up",
            "pageLink": "https://en-gb.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT1mBw3xnOtt79LekDH3pSkzEKQxpH8DaJ61QRI-lI4EaUBmJbgPWrUhkq7UeChNSUdaPvbmST7ZshGnlsBmBWKx3-XlR411ppk61ovK_EhtJ4quTG5NymC2f9ExoPi1NPTK99gIy1JGhUGF",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "‫Facebook - لاگ ان کریں یا سائن اپ کریں‬",
            "pageLink": "https://ur-pk.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT0gGctlc2qGxSGa-ix7wCN0fLUE1HcyyygoVZUlWsifHV_oZfHmvknrFVFP-xZ56dzpiBZ8Fd9su9XFAepC9SlTdTX98iyn5XH4t_sIuh1jNsKCNIp-Bi9_tgsjSXd2NmEbt3ZQQNmoff0q",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "मैं यह कैसे एडजस्ट करूँ कि Facebook पर और उसके बाहर मेरी एक्टिविटी के हिसाब से मुझे किस तरह के विज्ञापन � | Facebook मदद केंद्र | Facebook",
            "pageLink": "https://www.facebook.com/help/568137493302217",
            "imageCount": 3,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.intern.facebook.com/help/195227921252400?helpref=faq_content",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://en-gb.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "http://l.facebook.com/l.php?u=http%3A%2F%2Fwww.youronlinechoices.eu%2F&h=AT3AIqC4izzJXe0OFMYoZa4NHVZR4ATXW7cJ1TbhNE0xNOE7Ff13nCwF1oaMI1mWTdOnOyHxT2259Us6_sRXl9gBM1-1C2_HBAd5RkKitvNSDTkcfBJ_kdDakyZ5VuqPTW9YeIIwI_c-ybom",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "http://l.facebook.com/l.php?u=http%3A%2F%2Foptout.aboutads.info%2F&h=AT3sH29h9xA8EoheBEsS99Fit_7nxIE54TGaiMyPcJn_3d-7pH-5m_Jpc9vvUpsRXMPWgtBXsCPXDIYs44sIAefvOpBWQD3k670ShP8C793DucLpSYM_FkhhRoUApYzb1dipMmu4p10hjo1p",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://te-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/about/ads",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ur-pk.facebook.com/help/568137493302217?query=Java",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "http://l.facebook.com/l.php?u=http%3A%2F%2Fyouradchoices.ca%2F&h=AT0NqB5VxgMQZpIZcxbxzJEszyDAkG2Zrlg3roAqf8HqQoJA4vwfjAhWes-nMRglszU1NLTcVOatso_TBM_--JPEvpJWv2stD4rD9zwou-a5PFBx_YSrW_xo_JQPhef97spZuGKiukz8X8Q1",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - লগ ইন করুন অথবা নিবন্ধন করুন",
            "pageLink": "https://bn-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT0-jsfb-GOTSdES6TmlVfjlSk3FQV8pESWG_dWc4KTk5uwOyLI1bMaDZlpt0C8XrOYcsSqdKHJB5UnTwoMJFsNte5GAw4v9a2qNt3avDxk2q8RsCzv2wBTMgyQnrCf2Mcy89AwKZCWV9DFv",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://bn-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook Watch",
            "pageLink": "https://www.facebook.com/watch/",
            "imageCount": 11,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/nishamadhulika/videos/%E0%A4%86%E0%A4%B2%E0%A5%82-%E0%A4%B8%E0%A5%82%E0%A4%9C%E0%A5%80-%E0%A4%95%E0%A5%87-%E0%A4%95%E0%A5%81%E0%A4%B0%E0%A4%95%E0%A5%81%E0%A4%B0%E0%A5%87-%E0%A4%AB%E0%A4%BF%E0%A4%82%E0%A4%97%E0%A4%B0-%E0%A4%AB%E0%A5%8D%E0%A4%B0%E0%A4%BE%E0%A4%87%E0%A4%9C-potato-finger-fires-potato-rava-finger-fries-in/161623515292561/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/rtenews/videos/taoiseachs-statement-on-covid-19-restrictions/749943438875035/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/zeemusiccompany/videos/jaha-tum-rahoge-maheruh-amit-dolawat-drisha-more-altamash-faridi-kalyan-bhardhan/803549493436535/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/tacaz.official/videos/king-of-pubg-mobile-solo-king-vs-pro-squad/854705175033958/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/zeemusiccompany/videos/channa-ve-full-video-bhoot-part-one-the-haunted-ship-vicky-k-bhumi-p-akhil-mansh/669935727072127/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/Darkness429/videos/%E3%83%BD%E0%BA%88%D9%84%CD%9C%E0%BA%88%EF%BE%89-happy-friday/2675693252751976/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/NowThisPolitics/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/XiiaoCong4896/videos/1-top-game-playnocamper/762493164286419/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/ros5ihd/videos/999999-confirmed-hardest-ever-gta-challenges-with-massive-cash-drop/662135267900059/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/BindassKavyaPUBG/videos/hi-im-is-live-on-facebook-to-play-with-you/572694943377821/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/kanakskitchen/videos/easy-mango-ice-cream-recipe/3018434171528187/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/tseriesmusic/videos/jism-from-jism-2-arko-pravo-mukherjee/467512490645195/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/gamegurusky/videos/max-100-rp-road-to-conqueror-season-12-conqueror/539335053643052/?__so__=discover&__rv__=video_home_www_live_gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/deepaksart/videos/2x-slower-httpsyoutubeueyf2yrk364/566443210809143/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/NowThisPolitics/videos/worlds-largest-flower-blooms-smells-like-dead-meat/2762455017196974/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/tseriesmusic/videos/jan-gan-man-from-krazzy-4-rajesh-roshan/2447261578660974/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/zeemusiccompany/videos/pyaar-de-sunny-leone-rajniesh-duggall-ankit-tiwari-beiimaan-love/491459551705608/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/NovenNonVeg/videos/birthday-cake-in-lock-down-without-cream-egg-oven-noven/2700514803511237/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/cbcnews/videos/covid-19-update-trudeau-addresses-canadians-special-coverage/2295216367451630/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/NovenNonVeg/videos/banana-bread-in-lock-down-eggless-whole-wheat-banana-bread-super-soft-banana-bre/253060839175402/?__so__=discover&__rv__=popular_videos",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/aajtak/videos/%E0%A4%AE%E0%A4%82%E0%A4%A6%E0%A4%BF%E0%A4%B0-%E0%A4%95%E0%A5%87-%E0%A4%B8%E0%A5%8B%E0%A4%A8%E0%A5%87-%E0%A4%AA%E0%A4%B0-24-%E0%A4%95%E0%A5%88%E0%A4%B0%E0%A5%87%E0%A4%9F-%E0%A4%B5%E0%A4%BE%E0%A4%B2%E0%A5%80-%E0%A4%B8%E0%A4%BF%E0%A4%AF%E0%A4%BE%E0%A4%B8%E0%A4%A4-%E0%A4%A6%E0%A5%87%E0%A4%96%E0%A4%BF%E0%A4%8F-specialreport-anjana-om-kashyap-%E0%A4%95%E0%A5%87-%E0%A4%B8/2630746177141125/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/tseriesmusic/videos/kaise-kahein-alvida-from-yeh-saali-zindagi-arunoday-singh-javed-ali/1255590361279486/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/AkoSiDogie/videos/515-party-na-tara-gamitin-natin-indo-meta-pang-durog-/263526928034120/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/tseriesmusic/videos/haiyya-re-from-tara-the-journey-of-love-passion-prakash-prabhakar/792972867783727/?__so__=discover&__rv__=premium_music_video",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Fyoutu.be%2Fueyf2yrk364&h=AT3e5A1134glymTHdAk8kb5oJ-2Ol-Q9hPMq1oaAghhjTsxUPR7YvrwgJi3zTU0flK2GTpzYcN1E-etfZS_3t-pNxOhHRIjMOUZQ2pqCdhKqDry-_TK_spwFrRpZPXM1l5GgO1ni0KYtZSMS",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/NowThisPolitics/videos/house-debates-3-trillion-relief-bill/610357483023350/?__so__=discover&__rv__=top_live",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - लॉग इन किंवा साइन अप",
            "pageLink": "https://mr-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT07EDE9vyuQcgAClJR8fQYVe2bJ3wkdkTCIbb1_Ix9YTYRaP-qa9xyYCw_QDbAdYDrx_feB9VZDm03FjeAUis3fIbW3oCAdwX0IAfxcrJf1MYdhW8Pl3xFSoFw2RjTl0CBfyytD779ojpHN",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://mr-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - உள்நுழையவும் அல்லது பதிவுசெய்யவும்",
            "pageLink": "https://ta-in.facebook.com/?query=Java",
            "imageCount": 2,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/recover/initiate/?ars=facebook_login",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT13XcNDOiaZTaT8owSmLVBJggZnGQM5ZcBQX01caYMNQVXn9aeNDKNR02LUCAWNrxJ6OQ0jBVLiGzKAtwSY2aVXoalnxY70MQNc3nh12i6wz_c690EwCrsWI7JYOfhPWDFzM4chFkUvNRVP",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ta-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - ലോഗിൻ ചെയ്യുക അല്ലെങ്കില് സൈനപ്പ് ചെയ്യുക",
            "pageLink": "https://ml-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT34DrbO_sC-1KfVZ-un4sGaXRu5pO_aRKnNuOWHyEiVfx8M7jEDeDZmlJnQNtXbR3VqXZ06H3ifo_9XFTodDpdSxuJaUiwHt-igGlje-TJGYdPN5PV8fl29xJD-XEQllppAj4xP4US1JhPS",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://ml-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "",
            "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT1UUMBtjfN0iu4Nh81VFha2-unGo_rSrm5j61haWyyCaSKfXX6neg-Kgw3T1O09pwSSO3cad1B909mR3FRrn7TjWVKVZDD8bYM-zHlGGyNooNIK6E099mSTktwfAL-HpQ6gU3UK_adBRSSq",
            "imageCount": 0,
            "childPages": null,
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - लॉग इन या साइन अप करें",
            "pageLink": "https://www.facebook.com/",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT27QVjQMvpz2_Q9Bodo6YKQpX1vN2YjNwVbm3SGNS1Nr6fQt45mk54cgWLcar6zpYDZTBGOuqCAR44g8J1-9NPsOPAx-qELwbFOqk5HO9teOioB6cqcDBt12EjxAnFCWGFSPAAyeT8Cvy5A",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - ಲಾಗ್ ಇನ್ ಅಥವಾ ಸೈನ್ ಅಪ್",
            "pageLink": "https://kn-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT2BDNaRlotkXdyPLpmrQr01gyAjOUhZgWYhyd2lu6WYJbicEFQCS5xvyPxlz1flsTf7_Ok2RSLRpniukeqp301g3cKbmYiSLO6TJuyQVgeZGePxsaiSInSirg1dORjj3PFVSpyf3F3H8k3g",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://kn-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook - ਲੌਗ ਇਨ ਕਰੋ ਜਾਂ ਸਾਈਨ ਅੱਪ ਕਰੋ",
            "pageLink": "https://pa-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT2tp5-jcNqn90_AKwIYJNpuImQ1RdnqtIZj5lxh9hBUlw-wx1ZgOISdmRj1-p3JASe7jX5qgOHJb1ArU_K9l5yKLxOo7PlfhuTPKfOOOngEsAnfEnWZKcRLXfGUZEvOQ6A4az94NOv4ZUQD",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://pa-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": null,
            "pageLink": null,
            "imageCount": 0,
            "childPages": null,
            "currLevel": 0
        },
        {
            "fieldType": "Page",
            "pageTitle": "Facebook for Developers",
            "pageLink": "https://developers.facebook.com/?ref=pf",
            "imageCount": 8,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#gaming",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#publishing",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/blog/post/2020/05/06/how-startups-pivoting-during-epidemic/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#business-tools",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#social-integrations",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#open-source",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#augmented-reality",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#virtual-reality",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.f8.com%2F&h=AT2zl8kO3H9mdWIHuXgt1sgls1-NF52rB4KH7N3vOPdPODhn4HEMJSK5EGcSL6v_3AFKnQ0hbYQof-iwcvSXLBOqWnpvrcEQFZ5XSNFdTQ5K5L3jIiNKmELisrZekahXNqtIfBynn0rJl81R",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#social-presence",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/startups/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/blog/post/2020/05/14/making-apps-easier-access-as-demand-surges/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/blog/post/2020/05/05/introducing-graph-api-v7-marketing-api-v7/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products?ref=dfc_hero_fb4d",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://developers.facebook.com/products#artificial-intelligence",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Fpytorch.org%2F&h=AT1T6pnhUPwtp_pgzddq227yRziUKuCloFQB4PfX4GsR3oQtjBzxLtaaM5yVMURVPrt7YZiJechf4kW_OIrEFrdGBIaMHYFmHyEhIILX61XPa2gI9pmYoqSsuDnJI5TxdsVY042mt7LfrRN6",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "ફેસબુક-લોગ ઇન અથવા સાઈન અપ",
            "pageLink": "https://gu-in.facebook.com/?query=Java",
            "imageCount": 4,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.messenger.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/help/568137493302217",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F&h=AT3Z0e7wBepTuqQ_oZOzbfPjYHf-IAFQYhwiGxNUgAdu6_P05Pp_i1_04gxotMULTrn8rWZn5sJrb6qugj9EHco7T4bwUBsfrNLZZdndJauExNARvUNayhUyBTgzKHUjjietla19cghbSdmW",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/recover/initiate?lwv=110&ars=royal_blue_bar",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://gu-in.facebook.com/watch/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        },
        {
            "fieldType": "Page",
            "pageTitle": "Smart Video Calling with Alexa Built-in | Portal from Facebook",
            "pageLink": "https://portal.facebook.com/",
            "imageCount": 6,
            "childPages": [
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fportalfromfacebook%2F&h=AT2d8s-1Nho1J-7Fhom3bxmydeegE4TpJGhKkE1WBGc-UxyYf__ZB9b_xJ_JOgQsgvu_CK0te9FiczxXTHaPoztLve73CmnlnDNaVWUk2zOnzeu8DhV-YJ-c83K-g_e8ed-BjWP_dP-1NAWE",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/help/568137493302217/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/policy/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.instagram.com%2Fportalfromfacebook%2F&h=AT3dXcwQp7iXcYG1DD-zZCOKrthjqAR23buT0avqZWfGxcDNC5MrW51pWmwPZOT3ZkaN72YqqpotOEZtg_XYDODLuTSDC1ddfAmIFlEEWT-8TiFgJjDPeUJkB2ad4JigG-Tctc3slSaxzVIg",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/policies/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/policies/cookies/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://l.facebook.com/l.php?u=https%3A%2F%2Ftwitter.com%2FPortalFacebook%2F&h=AT0uXcC3Vp4NTwzGLr6OorsbP-dhbp-gYCUFZ8TU3hklARAyd7ASXnV1_h6eHIPMbP4mq0eRre53JAg6PT3yea6rmXcEQbRmDu709CkjQLIFUWfyJ4V8NKij6QhsS0AErOpVqsLQyfYEcsLV",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                },
                {
                    "fieldType": "Page",
                    "pageTitle": null,
                    "pageLink": "https://www.facebook.com/PortalFromFacebook/",
                    "imageCount": 0,
                    "childPages": null,
                    "currLevel": 0
                }
            ],
            "currLevel": 1
        }
    ],
    "uniqueId": "CRAWL6666481308903215104"
}





