package com.webapp.crawler.exception;

public class CrawlerException extends RuntimeException {

	private static final long serialVersionUID = 3746951378297949885L;

	private CrawlerExceptionEnum errorCode;

	public CrawlerException(String message) {
		super(message);
	}

	public CrawlerException(CrawlerExceptionEnum errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public CrawlerExceptionEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(CrawlerExceptionEnum errorCode) {
		this.errorCode = errorCode;
	}

}
