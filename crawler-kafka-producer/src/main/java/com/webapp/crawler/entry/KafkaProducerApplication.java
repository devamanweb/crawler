package com.webapp.crawler.entry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
@ComponentScan({"com.webapp.crawler*"})
public class KafkaProducerApplication implements EnvironmentAware{

	public static void main(String[] args) {
		SpringApplication.run(KafkaProducerApplication.class, args);
	}

	@LoadBalanced
	@Bean
	public RestTemplate loadbalancedRestTemplate() {
	     return new RestTemplate();
	}

	@Override
	public void setEnvironment(Environment environment) {
		Properties props = new Properties();
		try {
			props.setProperty("crawlerapp.server.address", InetAddress.getLocalHost().getHostAddress() + ".client");
			PropertiesPropertySource propertySource = new PropertiesPropertySource("myProps", props);
			if (environment instanceof StandardEnvironment) {
				((StandardEnvironment) environment).getPropertySources().addFirst(propertySource);
			}
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

}
