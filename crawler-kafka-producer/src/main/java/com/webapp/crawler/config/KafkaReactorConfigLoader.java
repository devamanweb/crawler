package com.webapp.crawler.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KafkaReactorConfigLoader {
	protected static final Logger LOGGER = LoggerFactory.getLogger(KafkaReactorConfigLoader.class);

	private KafkaYmlConfig kafkaYmlConfig;


	@Autowired
	public KafkaReactorConfigLoader(KafkaYmlConfig kafkaYmlConfig) {
		this.kafkaYmlConfig = kafkaYmlConfig;
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaYmlConfig.getServer());
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, kafkaYmlConfig.getCompressionType());
		props.put(ProducerConfig.RETRIES_CONFIG, Integer.MAX_VALUE);
		props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, kafkaYmlConfig.getSecurityProtocol());
	}
}
