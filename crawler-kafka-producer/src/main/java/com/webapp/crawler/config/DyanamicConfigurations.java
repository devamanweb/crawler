package com.webapp.crawler.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Configuration
@ConfigurationProperties()
public class DyanamicConfigurations {

	
	int duplicatePacketCheckEnabled;
	List<String> mojoDisabledProcess;

	public int getDuplicatePacketCheckEnabled() {
		return duplicatePacketCheckEnabled;
	}

	public void setDuplicatePacketCheckEnabled(int duplicatePacketCheckEnabled) {
		this.duplicatePacketCheckEnabled = duplicatePacketCheckEnabled;
	}

	public List<String> getMojoDisabledProcess() {
		return mojoDisabledProcess;
	}

	public void setMojoDisabledProcess(List<String> mojoDisabledProcess) {
		this.mojoDisabledProcess = mojoDisabledProcess;
	}
}
