package com.webapp.crawler.service.impl;

import com.webapp.crawler.duplicatepacket.DuplicatePacketAdapter;
import com.webapp.crawler.duplicatepacket.DuplicatePacketResponse;
import com.webapp.crawler.exception.CrawlerException;
import com.webapp.crawler.exception.CrawlerExceptionEnum;
import com.webapp.crawler.producer.response.vo.GenericResponse;
import com.webapp.crawler.producer.response.vo.ResponseContract;
import com.webapp.crawler.util.hashcodegenerator.GenericHashCodeGenerator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.webapp.crawler.constants.Constants;
import com.webapp.crawler.config.KafkaReactorConfigLoader;
import com.webapp.crawler.service.KafkaBaseService;
import com.webapp.crawler.entities.containers.RequestContract;

/*
 * This class strictly follows open close principal.
 * If any modification need to be done which is client specific,
 * please extend the class and override the method instead of writing if else conditions in code.
 */
@Service
public class GenericReactorKafkaService extends KafkaBaseService {
	@Autowired
	protected GenericHashCodeGenerator genericHashCodeGenerator;

	public GenericReactorKafkaService(KafkaReactorConfigLoader kafkaReactorConfigLoader,
									  DuplicatePacketAdapter duplicatePacketAdapter, Gson gson) {
		super(kafkaReactorConfigLoader, duplicatePacketAdapter, gson);
	}

	public String[] getIdentifier(String uniqueId) {
		if (StringUtils.isNotBlank(uniqueId) && uniqueId.contains(Constants.UNIQUE_ID_SEPERATOR)
				&& !uniqueId.endsWith(Constants.UNIQUE_ID_SEPERATOR)) {
			return uniqueId.split(Constants.UNIQUE_ID_SEPERATOR);
		}
		return  new String[]{uniqueId,uniqueId,uniqueId};
	}


	protected String getFailureUniqueId(Object requestVo) {
		return ((RequestContract) requestVo).getUniqueId();
	}

	protected ResponseContract createResponse(RequestContract requestContract, boolean isFailed) {
		GenericResponse responseContract = new GenericResponse();
		responseContract.setToken(requestContract.getUniqueId());
		responseContract.setStatus(isFailed);
		return responseContract;
	}

	@Override
	protected void validate(RequestContract requestVo) {
		if (dyanamicConfigurations.getDuplicatePacketCheckEnabled() == 1) {
			LOGGER.debug("{} - Duplicate Packet Check is enabled ..", requestVo.getUniqueId());
			checkForDuplicacyAndSetGeneratedHashCode(requestVo);
		} else {
			LOGGER.debug("{} - Duplicate Packet Check is disabled.... Going to directly send the kafka packet",
					requestVo.getUniqueId());
		}
	}

	protected void checkForDuplicacyAndSetGeneratedHashCode(RequestContract requestVo) {
		DuplicatePacketResponse duplicatePacketResponse = duplicatePacketAdapter.checkForDuplicatePacket(requestVo);
		requestVo.setGeneratedHashCode(duplicatePacketResponse.getGeneratedHashCode());
		if (requestVo.getGeneratedHashCode() == null) {
			requestVo.setGeneratedHashCode(genericHashCodeGenerator.generateHashCode(requestVo));
		}
		if (duplicatePacketResponse.getIsRequestDuplicate()) {
			LOGGER.info("{} - Duplicate Packet found hence rejecting the request!!", requestVo.getUniqueId());
			throw new CrawlerException(CrawlerExceptionEnum.DUPLICATE_PACKET_FOUND,
					requestVo.getUniqueId() + " - Duplicate Packet found hence rejecting the request!!");
		}
	}

	@Override
	protected String getSuccessUniqueId(RequestContract requestVo) {
		return requestVo.getUniqueId();
	}
}
