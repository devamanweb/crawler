package com.webapp.crawler.service.impl;

import com.google.gson.Gson;
import com.webapp.crawler.config.KafkaReactorConfigLoader;
import com.webapp.crawler.duplicatepacket.DuplicatePacketAdapter;
import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.service.KafkaBaseService;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Service;

@Service
public class CrawlerKafkaService extends GenericReactorKafkaService {

	public CrawlerKafkaService(KafkaReactorConfigLoader kafkaReactorConfigLoader,
							   DuplicatePacketAdapter duplicatePacketAdapter, Gson gson) {
		super(kafkaReactorConfigLoader, duplicatePacketAdapter, gson);
	}

	@Override
	protected String getSuccessUniqueId(RequestContract requestVo) {
		return super.getSuccessUniqueId(requestVo) ;
	}

	@Override
	protected String getFailureUniqueId(Object requestVo) {
		return super.getFailureUniqueId(requestVo) ;
	}


	@Override
	protected void validate(RequestContract requestVo) {
		CrawlerRequestContract crawlerRequestContract = (CrawlerRequestContract) requestVo;

		UrlValidator urlValidator = new UrlValidator();

		if (!urlValidator.isValid(crawlerRequestContract.getUrl())) {
			KafkaBaseService.LOGGER.info("{} - Validation failed hence rejecting the request!!", requestVo.getUniqueId());
			throw new RuntimeException("Validation failed hence rejecting the request!");
		}
		super.validate(requestVo);
	}

}
