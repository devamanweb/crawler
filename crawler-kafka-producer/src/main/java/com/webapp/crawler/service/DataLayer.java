package com.webapp.crawler.service;

import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.entities.response.vo.CrawlerResponse;
import com.mongodb.*;
import com.mongodb.util.JSON;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class DataLayer {

    MongoClient mongo = new MongoClient("localhost", 27017);
    DB db = mongo.getDB("mydatabase");
    public void insertJson(String  jsonStr, String collectionName) {

        //create request

        DBObject doc = (DBObject) JSON
                .parse(jsonStr);
        DBCollection col = db.getCollection(collectionName);
        WriteResult result = col.insert(doc);

        System.out.println(result.getUpsertedId());
        System.out.println(result.getN());
        System.out.println(result.isUpdateOfExisting());

    }



    public String findByUniqueId( String collectionName, String uniqueId) {

        //read example
        DBCollection col = db.getCollection(collectionName);
        DBObject query = BasicDBObjectBuilder.start().add("uniqueId",uniqueId).get();
        DBObject dbObject = col.findOne(query);
        JSONObject output = new JSONObject(JSON.serialize(dbObject));
        return output.toString();


    }

    public String findRequestStatusUniqueId( String uniqueId) {
       return findByUniqueId("CrawlerRequest",uniqueId);
    }

    public String findResponseByUniqueId( String uniqueId) {
        return findByUniqueId("CrawlerResponse",uniqueId);
    }

    public void updateStatus(String collectionName, String status, String uniqueId){

        BasicDBObject updateDocument = new BasicDBObject();
        updateDocument.append("$set", new BasicDBObject().append("status", status));

        BasicDBObject searchQuery2 = new BasicDBObject().append("uniqueId", uniqueId);
        DBCollection col = db.getCollection(collectionName);
        col.update(searchQuery2, updateDocument);

    }

    private static DBObject createDBObject(CrawlerRequestContract crawlerRequestContract) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("url", crawlerRequestContract.getUrl());
        docBuilder.append("depth", crawlerRequestContract.getDepth());
        docBuilder.append("uniqueId", crawlerRequestContract.getUniqueId());
        return docBuilder.get();
    }

    private static DBObject createDBObject(CrawlerResponse crawlerResponse) {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("totalLinks", crawlerResponse.getTotalLinks());
        docBuilder.append("totalImages", crawlerResponse.getTotalImages());
        docBuilder.append("uniqueId", crawlerResponse.getPageDetails());
        return docBuilder.get();
    }


}
