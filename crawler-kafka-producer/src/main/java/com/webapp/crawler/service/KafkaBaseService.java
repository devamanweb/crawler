package com.webapp.crawler.service;

import java.util.Properties;
import java.util.Random;

import com.webapp.crawler.config.KafkaYmlConfig;
import com.webapp.crawler.duplicatepacket.DuplicatePacketAdapter;
import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.producer.response.vo.ResponseContract;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.webapp.crawler.config.DyanamicConfigurations;
import com.webapp.crawler.config.KafkaReactorConfigLoader;
import com.webapp.crawler.util.SequenceGeneratorAdaptor;

public abstract class KafkaBaseService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(KafkaBaseService.class);
	protected DuplicatePacketAdapter duplicatePacketAdapter;
	protected Gson gson;

	@Autowired
	KafkaYmlConfig kafkaYmlConfig;

	@Autowired
	protected DyanamicConfigurations dyanamicConfigurations;

	@Autowired
	protected DataLayer dataLayer;

	@Autowired
	protected SequenceGeneratorAdaptor sequenceGeneratorAdaptor;


	@Autowired
	public KafkaBaseService(KafkaReactorConfigLoader kafkaReactorConfigLoader,
							DuplicatePacketAdapter duplicatePacketAdapter, Gson gson) {
		this.duplicatePacketAdapter = duplicatePacketAdapter;
		this.gson = gson;
	}

	public ResponseContract sendPacket(RequestContract requestContract) {
		validate(requestContract);
		long beforeInvokeMilis = System.currentTimeMillis();
		boolean isPushed = true;
		String topic = kafkaYmlConfig.getTopic();
		Gson g = new Gson();
		CrawlerRequestContract crawlerRequestContract = (CrawlerRequestContract)requestContract;
		try {
			Properties props = new Properties();
			props.put("bootstrap.servers", "localhost:9092");
			props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, kafkaYmlConfig.isEnableIdempotence());
			props.put(ProducerConfig.ACKS_CONFIG, kafkaYmlConfig.getAcks());
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
			props.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, kafkaYmlConfig.getCompressionType());
			props.put(ProducerConfig.RETRIES_CONFIG, Integer.MAX_VALUE);

			Producer producer = new KafkaProducer(props);
			sequenceGeneratorAdaptor.setRequestUniqueId(requestContract);
			Random random = new Random();
			int partition = random.nextInt(4)  ;
			ProducerRecord<Integer, String> producerRecord = new ProducerRecord(topic,partition,1,requestContract.toString());
			producer.send(producerRecord);
			crawlerRequestContract.setStatus("Submitted");
			producer.close();
		} catch (org.apache.kafka.common.errors.TimeoutException ex) {
			isPushed = false;
			crawlerRequestContract.setStatus("Failed");
			LOGGER.error("{} Recieved Time out error.. ", requestContract);
			handleExceptionCase(requestContract, topic, ex);
		} catch (Exception ex) {
			isPushed = true;
			handleExceptionCase(requestContract, topic, ex);
		}
		finally {
			dataLayer.insertJson(g.toJson(crawlerRequestContract),"CrawlerRequest");
			LOGGER.info("Time taken to push packet " ,System.currentTimeMillis() - beforeInvokeMilis);
		}

		return createResponse(requestContract, isPushed);
	}


	public ResponseContract sendPacketThread(RequestContract requestContract) {
		validate(requestContract);
		long beforeInvokeMilis = System.currentTimeMillis();
		boolean isPushed = true;
		String topic = kafkaYmlConfig.getTopic();
		Gson g = new Gson();
		CrawlerRequestContract crawlerRequestContract = (CrawlerRequestContract)requestContract;
		try {
			Properties props = new Properties();
			props.put("bootstrap.servers", "localhost:9092");
			props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
			props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

			Producer producer = new KafkaProducer(props);
			sequenceGeneratorAdaptor.setRequestUniqueId(requestContract);
			Random random = new Random();
			int partition = random.nextInt(4) +1 ;
			ProducerRecord<Integer, String> producerRecord = new ProducerRecord(topic,partition,1,requestContract.toString());
			producer.send(producerRecord);
			crawlerRequestContract.setStatus("Submitted");
			producer.close();
		} catch (org.apache.kafka.common.errors.TimeoutException ex) {
			isPushed = false;
			crawlerRequestContract.setStatus("Failed");
			LOGGER.error("{} Recieved Time out error.. ", requestContract);
			handleExceptionCase(requestContract, topic, ex);
		} catch (Exception ex) {
			isPushed = true;
			handleExceptionCase(requestContract, topic, ex);
		}
		finally {
			dataLayer.insertJson(g.toJson(crawlerRequestContract),"CrawlerRequest");
			LOGGER.info("Time taken to push packet " ,System.currentTimeMillis() - beforeInvokeMilis);
		}

		return createResponse(requestContract, isPushed);
	}


	private void handleExceptionCase(RequestContract requestContract, String topic, Exception ex) {
		LOGGER.error("{} Following Error Occured :", requestContract, ex);
		ex.printStackTrace();
	}

	protected abstract String getSuccessUniqueId(RequestContract requestVo);

	protected abstract ResponseContract createResponse(RequestContract requestContract, boolean isFailed);

	protected abstract void validate(RequestContract requestVo);

}
