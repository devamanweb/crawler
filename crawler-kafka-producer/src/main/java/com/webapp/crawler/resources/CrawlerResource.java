package com.webapp.crawler.resources;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webapp.crawler.service.DataLayer;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.entities.response.vo.CrawlerResponse;
import com.webapp.crawler.producer.response.vo.ResponseContract;
import com.webapp.crawler.config.DyanamicConfigurations;
import com.webapp.crawler.service.impl.CrawlerKafkaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/crawler")
public class CrawlerResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(CrawlerResource.class);

	CrawlerKafkaService crawlerKafkaService;
	DyanamicConfigurations dyanamicConfigurations;
	@Autowired
	DataLayer dataLayer;

	@Autowired
	public CrawlerResource(CrawlerKafkaService crawlerKafkaService,
						   DyanamicConfigurations dyanamicConfigurations) {
		super();
		this.crawlerKafkaService = crawlerKafkaService;
		this.dyanamicConfigurations = dyanamicConfigurations;
	}

	@RequestMapping(value = "/push", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ResponseContract> pushToKafka(@RequestBody CrawlerRequestContract requestContracts) {

		ResponseContract kafkaPushResponse  = crawlerKafkaService.sendPacket(requestContracts);

		return new ResponseEntity<ResponseContract>(kafkaPushResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity getStatus(@RequestParam(value = "token") String token) {
		String response = dataLayer.findRequestStatusUniqueId(token);
		CrawlerRequestContract crawlerRequestContract= null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			crawlerRequestContract = mapper.readValue(response, CrawlerRequestContract.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity(crawlerRequestContract.getStatus(), HttpStatus.OK);

	}

	@RequestMapping(value = "/result", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<CrawlerResponse> getResult(@RequestParam(value = "token") String token) {
		String response = dataLayer.findResponseByUniqueId(token);
		CrawlerResponse responseContract= null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			 responseContract = mapper.readValue(response, CrawlerResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CrawlerResponse>(responseContract, HttpStatus.OK);

	}
}
