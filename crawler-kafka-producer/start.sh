#!/bin/sh

#/usr/bin/nohup /opt/java/jdk8/bin/java -Dserver.port=80 -Djavax.net.ssl.keyStore=/opt/conf/client-mtf.jks -Djavax.net.ssl.keyStorePassword=password -Djavax.net.ssl.keyStoreType=JKS  -jar merchant-payment-service-1.0.jar &
/bin/su mmytu -c "/usr/bin/nohup java -jar crawler-producer.jar --spring.config.location=application-prod.yml &"
/bin/chmod 775 nohup.out
MyPID=$!                        # Record  PID
echo $MyPID
