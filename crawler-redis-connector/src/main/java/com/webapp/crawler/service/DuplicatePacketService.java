package com.webapp.crawler.service;

import java.util.concurrent.TimeUnit;

import com.webapp.crawler.util.hashcodegenerator.GenericHashCodeGenerator;
import com.webapp.crawler.redis.RedisDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webapp.crawler.config.DyanamicConfigs;
import com.webapp.crawler.response.vo.DuplicatePacketResponse;
import com.webapp.crawler.entities.containers.RequestContract;

@Service
public class DuplicatePacketService {

	private RedisDao reqDao;
	private GenericHashCodeGenerator genericHashCodeGenerator;
	private DyanamicConfigs dyanamicConfigs;
	private static final Logger LOGGER = LoggerFactory.getLogger(DuplicatePacketService.class);


	@Autowired
	public DuplicatePacketService(RedisDao reqDao, GenericHashCodeGenerator genericHashCodeGenerator,
			DyanamicConfigs dyanamicConfigs) {
		super();
		this.reqDao = reqDao;
		this.genericHashCodeGenerator = genericHashCodeGenerator;
		this.dyanamicConfigs = dyanamicConfigs;
	}

	public DuplicatePacketResponse checkRequestForDuplicacy(RequestContract request) {
		DuplicatePacketResponse redisResponse = new DuplicatePacketResponse();
		String redisKey = genericHashCodeGenerator.generateHashCode(request);
		redisResponse.setGeneratedHashCode(redisKey);
		redisResponse.setIsRequestDuplicate(isRequestDuplicate(request, redisKey));
		redisResponse.setStatus(true);
		return redisResponse;
	}

	public boolean isRequestDuplicate(RequestContract request, String redisKey) {
		try {
			String value = reqDao.retrievePacketAndUpdateTimeOut(redisKey, dyanamicConfigs.getDefaultTimeOut(),
					TimeUnit.SECONDS);
			LOGGER.info("{} - Following key was updated with timeout : {} key: {}", request.getUniqueId(),
					dyanamicConfigs.getDefaultTimeOut(), redisKey);
			if (value != null && value.equals("1")) {
				return true;
			}
			reqDao.pushPacketToRedis(redisKey, "1", dyanamicConfigs.getDefaultTimeOut(), TimeUnit.SECONDS);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error("{} - Following Exception Occured While checking for duplicate packet : {}",
					request.getUniqueId(), ex.toString());
		}
		return false;
	}

}
