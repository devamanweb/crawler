package com.webapp.crawler.service;

import java.util.concurrent.TimeUnit;

import com.webapp.crawler.redis.RedisDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webapp.crawler.request.vo.crudoperations.RedisCrudOperationsRequestVO;
import com.webapp.crawler.response.vo.RedisCrudOperationsResponseVO;
import com.webapp.crawler.response.vo.ResponseContract;

@Service
public class RedisCrudOperationService {
	private RedisDao redisDao;
	private static final Logger LOGGER = LoggerFactory.getLogger(RedisCrudOperationService.class);

	@Autowired
	public RedisCrudOperationService(RedisDao redisDao) {
		this.redisDao = redisDao;
	}

	public ResponseContract pushPacketToRedis(RedisCrudOperationsRequestVO requestVO) {
		LOGGER.debug("Going to push following packet in Redis -" + requestVO);
		RedisCrudOperationsResponseVO responseVO = new RedisCrudOperationsResponseVO();
		if (requestVO.getTimeOut() > 0) {
			redisDao.pushPacketToRedis(requestVO.getKey(), requestVO.getValue(), requestVO.getTimeOut(),
					TimeUnit.SECONDS);
		} else {
			redisDao.pusPacketToRedis(requestVO.getKey(), requestVO.getValue());
		}
		responseVO.setKey(requestVO.getKey());
		responseVO.setValue(requestVO.getValue());
		responseVO.setStatus(true);
		LOGGER.debug("Redis Packet push successful -" + requestVO);
		return responseVO;
	}

	public ResponseContract updatePacketinRedis(RedisCrudOperationsRequestVO requestVO) {
		return pushPacketToRedis(requestVO);
	}

	public ResponseContract getPacketFromRedis(RedisCrudOperationsRequestVO requestVO) {
		LOGGER.debug("Going to fetch from following packet from Redis -" + requestVO);
		String packet = "";
		RedisCrudOperationsResponseVO responseVO = new RedisCrudOperationsResponseVO();
		if (requestVO.getTimeOut() > 0) {
			packet = redisDao.retrievePacketAndUpdateTimeOut(requestVO.getKey(), requestVO.getTimeOut(),
					TimeUnit.SECONDS);
		} else {
			packet = redisDao.retrievePacketFromRedis(requestVO.getKey());
		}
		responseVO.setValue(packet);
		responseVO.setKey(requestVO.getKey());
		responseVO.setStatus(true);
		LOGGER.debug(requestVO + "- Received following packet from redis -" + packet);
		return responseVO;
	}

	public ResponseContract deletePacketFromRedis(RedisCrudOperationsRequestVO requestVO) {
		LOGGER.debug("Going to delete following packet from Redis -" + requestVO);
		RedisCrudOperationsResponseVO responseVO = new RedisCrudOperationsResponseVO();
		responseVO.setStatus(redisDao.deletePacketFromRedis(requestVO.getKey()));
		responseVO.setKey(requestVO.getKey());
		responseVO.setValue(requestVO.getValue());
		return responseVO;
	}
}
