package com.webapp.crawler.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Configuration
@ConfigurationProperties()
public class DyanamicConfigs {
    
	int defaultTimeOut;

	public int getDefaultTimeOut() {
		return defaultTimeOut;
	}

	public void setDefaultTimeOut(int defaultTimeOut) {
		this.defaultTimeOut = defaultTimeOut;
	}

}
