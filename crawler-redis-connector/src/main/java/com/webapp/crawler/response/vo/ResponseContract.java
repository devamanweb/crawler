package com.webapp.crawler.response.vo;

public class ResponseContract {

	boolean status;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
