package com.webapp.crawler.response.vo;

public class DuplicatePacketResponse extends ResponseContract{

	private Boolean isRequestDuplicate;
	private String generatedHashCode;

	public Boolean getIsRequestDuplicate() {
		return isRequestDuplicate;
	}

	public void setIsRequestDuplicate(Boolean isRequestDuplicate) {
		this.isRequestDuplicate = isRequestDuplicate;
	}

	public String getGeneratedHashCode() {
		return generatedHashCode;
	}

	public void setGeneratedHashCode(String generatedHashCode) {
		this.generatedHashCode = generatedHashCode;
	}

}
