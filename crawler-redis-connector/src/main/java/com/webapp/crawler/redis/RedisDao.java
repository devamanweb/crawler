package com.webapp.crawler.redis;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RedisDao {

	private StringRedisTemplate stringRedisTemplate;

	@Autowired
	RedisDao(StringRedisTemplate stringRedisTemplate) {
		this.stringRedisTemplate = stringRedisTemplate;
	}

	public void pushPacketToRedis(String key, String value, int timeout, TimeUnit timeUnit) {
		pusPacketToRedis(key, value);
		expirePacketFromRedis(key, timeout, timeUnit);
	}

	public String retrievePacketAndUpdateTimeOut(String key, int timeout, TimeUnit timeUnit) {
		String value = retrievePacketFromRedis(key);
		if (value != null)
			expirePacketFromRedis(key, timeout, timeUnit);
		return value;
	}

	public void pusPacketToRedis(String key, String value) {
		stringRedisTemplate.opsForValue().set(key, value);
	}

	public String retrievePacketFromRedis(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

	public void expirePacketFromRedis(String key, int timeout, TimeUnit timeUnit) {
		stringRedisTemplate.expire(key, timeout, timeUnit);
	}
	
	public boolean deletePacketFromRedis(String key) {
		return stringRedisTemplate.delete(key);
	}
}
