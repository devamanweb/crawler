package com.webapp.crawler.request.vo.crudoperations;

public class RedisCrudOperationsRequestVO {

	private String key;
	private String value;
	private int timeOut;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
}
