package com.webapp.crawler.resources;

import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.service.DuplicatePacketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.webapp.crawler.response.vo.DuplicatePacketResponse;

@RestController
@RequestMapping("/v1/checkDuplicatePacket")
public class DuplicatePacketResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(DuplicatePacketResource.class);

	DuplicatePacketService duplicatePacketService;

	@Autowired
	public DuplicatePacketResource(DuplicatePacketService duplicatePacketService) {
		super();
		this.duplicatePacketService = duplicatePacketService;
	}

	@RequestMapping(value = "/crawler", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<DuplicatePacketResponse> checkDuplicatePacketMojo(@RequestBody RequestContract mojoRequest) {
		DuplicatePacketResponse redisResponse = duplicatePacketService.checkRequestForDuplicacy(mojoRequest);
		return new ResponseEntity<DuplicatePacketResponse>(redisResponse, HttpStatus.OK);
	}

}