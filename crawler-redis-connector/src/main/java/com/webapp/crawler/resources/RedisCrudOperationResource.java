package com.webapp.crawler.resources;

import com.webapp.crawler.request.vo.crudoperations.RedisCrudOperationsRequestVO;
import com.webapp.crawler.response.vo.ResponseContract;
import com.webapp.crawler.service.RedisCrudOperationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class RedisCrudOperationResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisCrudOperationResource.class);

	private RedisCrudOperationService redisCrudOperationServce;

	@Autowired
	public RedisCrudOperationResource(RedisCrudOperationService redisCrudOperationServce) {
		super();
		this.redisCrudOperationServce = redisCrudOperationServce;
	}

	@RequestMapping(value = "/pushPacket", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ResponseContract> pushToRedis(@RequestBody RedisCrudOperationsRequestVO requestVO) {
		LOGGER.info("Request Recieved - " + requestVO);
		ResponseContract pushPacketToRedis = redisCrudOperationServce.pushPacketToRedis(requestVO);
		return new ResponseEntity<ResponseContract>(pushPacketToRedis, HttpStatus.OK);
	}

	@RequestMapping(value = "/updatePacket", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<ResponseContract> updateRedisPacket(@RequestBody RedisCrudOperationsRequestVO requestVO) {
		LOGGER.info("Request Recieved - " + requestVO);
		ResponseContract pushPacketToRedis = redisCrudOperationServce.updatePacketinRedis(requestVO);
		return new ResponseEntity<ResponseContract>(pushPacketToRedis, HttpStatus.OK);
	}

	@RequestMapping(value = "/retrievePacket", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseContract> retrieveRedisPacket(@RequestBody RedisCrudOperationsRequestVO requestVO) {
		LOGGER.info("Request Recieved - " + requestVO);
		ResponseContract pushPacketToRedis = redisCrudOperationServce.getPacketFromRedis(requestVO);
		return new ResponseEntity<ResponseContract>(pushPacketToRedis, HttpStatus.OK);
	}

	@RequestMapping(value = "/deletePacket", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<ResponseContract> deleteRedisPacket(@RequestBody RedisCrudOperationsRequestVO requestVO) {
		LOGGER.info("Request Recieved - " + requestVO);
		ResponseContract pushPacketToRedis = redisCrudOperationServce.deletePacketFromRedis(requestVO);
		return new ResponseEntity<ResponseContract>(pushPacketToRedis, HttpStatus.OK);
	}

}