package com.webapp.crawler.config;

import org.springframework.stereotype.Component;

@Component
public class ConsumerConfig {
    private String host;
    private int port;
    private String groupId;
    private String autoOffsetReset;
    private String topic;
    private int noOfConsumers;
    private String compressionType;
    private String securityProtocol;

    public int getNoOfConsumers() {
        return noOfConsumers;
    }

    public void setNoOfConsumers(int noOfConsumers) {
        this.noOfConsumers = noOfConsumers;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAutoOffsetReset() {
        return autoOffsetReset;
    }

    public void setAutoOffsetReset(String autoOffsetReset) {
        this.autoOffsetReset = autoOffsetReset;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(String compressionType) {
        this.compressionType = compressionType;
    }

    public String getSecurityProtocol() {
        return securityProtocol;
    }

    public void setSecurityProtocol(String securityProtocol) {
        this.securityProtocol = securityProtocol;
    }
}
