package com.webapp.crawler.config;

import com.webapp.crawler.constants.AppConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = AppConstants.APP_CONFIG)
public class AppConfig {
    private ConsumerConfig consumerCrawlerConfig;

    public ConsumerConfig getConsumerCrawlerConfig() {
        return consumerCrawlerConfig;
    }

    public void setConsumerCrawlerConfig(ConsumerConfig consumerCrawlerConfig) {
        this.consumerCrawlerConfig = consumerCrawlerConfig;
    }
}
