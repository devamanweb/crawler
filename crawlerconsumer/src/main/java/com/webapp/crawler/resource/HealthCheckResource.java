package com.webapp.crawler.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckResource {

	@RequestMapping("/actuator/health")
	private String testHealth() {

		return "UP";
	}
}
