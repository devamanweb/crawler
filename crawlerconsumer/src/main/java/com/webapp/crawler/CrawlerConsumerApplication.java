package com.webapp.crawler;

import com.webapp.crawler.services.CrawlerMainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan({"com.webapp.crawler"})
public class CrawlerConsumerApplication implements CommandLineRunner, EnvironmentAware {

    @Autowired
    CrawlerMainService crawlerMainService;


    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(CrawlerConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        crawlerMainService.perform();
    }

    @LoadBalanced
    @Bean
    public RestTemplate loadbalancedRestTemplate() {
        return new RestTemplate();
    }

    @Override
    public void setEnvironment(Environment environment) {
        Properties props = new Properties();
        try {
            props.setProperty("crawlerapp.server.address", InetAddress.getLocalHost().getHostAddress() + ".client");
            PropertiesPropertySource propertySource = new PropertiesPropertySource("myProps", props);
            if (environment instanceof StandardEnvironment) {
                ((StandardEnvironment) environment).getPropertySources().addFirst(propertySource);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
