package com.webapp.crawler.services;

import com.webapp.crawler.entities.response.vo.Page;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


@Component
public class CrawlerLoader {


    public Page loadPageDetails(String url, int level, UrlValidator urlValidator, Set<String> allLinks)  {
        Page page = new Page();
            try {
                Set<String> links = new HashSet<>();
                System.out.println("URL is " + url);
                Document doc = Jsoup.connect(url)
                        .data("query", "Java")
                        .userAgent("Mozilla")
                        .cookie("auth", "token")
                        .timeout(3000)
                        .get();

                if (level > 0) {
                    Elements elements = doc.select("a[href]");
                    for (Element element : elements) {
                        links.add(element.attr("href"));
                    }
                }

                String title = doc.title();
                Elements img = doc.getElementsByTag("img");

                page.setPageLink(url);
                page.setImageCount(img.size());
                page.setPageTitle(title);
                page.setCurrLevel(level);
                if (links.size() != 0)
                    addChildPages(page, links, level, urlValidator, allLinks);
            }catch (Exception e){

            }

        return page;
    }

    void addChildPages(Page p, Set<String> links,int level, UrlValidator urlValidator, Set<String> allLinks) {

        links.removeAll(allLinks);
        Set<String> invalidLinks = new HashSet<>();
        Iterator<String> itr = links.iterator();

        while (itr.hasNext()) {

            String next = itr.next();
            //Check if valid url is there.
            if (validateURL(next,urlValidator)) {
                Page child = new Page();
                child.setCurrLevel(level - 1);
                child.setImageCount(0);
                child.setPageLink(next);
                if (p.getChildPages() == null) {
                    p.setChildPages(new HashSet<Page>());
                }
                p.getChildPages().add(child);
            }else{
                invalidLinks.add(next);
            }
        }
        links.removeAll(invalidLinks);
        allLinks.addAll(links);
    }


    boolean validateURL(String url, UrlValidator urlValidator){
       return urlValidator.isValid(url);

    }


}
