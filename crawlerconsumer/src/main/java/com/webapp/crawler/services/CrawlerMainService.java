package com.webapp.crawler.services;

import com.google.gson.Gson;
import com.webapp.crawler.DataLayer;
import com.webapp.crawler.config.AppConfig;
import com.webapp.crawler.entities.response.vo.Page;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.entities.response.vo.CrawlerResponse;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;

@Component
public class CrawlerMainService {


    AppConfig appConfig;

    @Autowired
    CrawlerLoader crawlerLoader;

    @Autowired
    DataLayer dataLayer;


    @Autowired
    public CrawlerMainService( AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public void perform() {
        try {
            com.webapp.crawler.config.ConsumerConfig crawlerConfig = appConfig.getConsumerCrawlerConfig();
            //Creating consumer properties
            Properties properties=new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,crawlerConfig.getHost() + ":" + crawlerConfig.getPort());
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,   IntegerDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,crawlerConfig.getGroupId());
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,crawlerConfig.getAutoOffsetReset());
            //creating consumer
            KafkaConsumer<Integer,String> consumer= new KafkaConsumer<Integer,String>(properties);
            //Subscribing
            consumer.subscribe(Arrays.asList(crawlerConfig.getTopic()));
            UrlValidator urlValidator = new UrlValidator();

            //polling
            while(true){
                ConsumerRecords<Integer,String> records=consumer.poll(Duration.ofMillis(100));
                for(ConsumerRecord<Integer,String> record: records) {
                    Gson g = new Gson();
                    CrawlerRequestContract crawlerRequestContract = null;

                    CrawlerResponse crawlerResponse = new CrawlerResponse();
                    LOGGER.info("Key: " + record.key() + ", Value:" + record.value());
                    LOGGER.info("Partition:" + record.partition() + ",Offset:" + record.offset());

            try {

                        crawlerRequestContract = g.fromJson(record.value(), CrawlerRequestContract.class);
                        dataLayer.updateStatus("CrawlerRequest","In-Process", crawlerRequestContract.getUniqueId());

                        Stack<Page> pageStack = new Stack<Page>();
                        Set<String> allLinks = new HashSet<>();
                        Page basePage = crawlerLoader.loadPageDetails(crawlerRequestContract.getUrl(), crawlerRequestContract.getDepth(), urlValidator, allLinks);
                        //Create response object and set values

                        Iterator<Page> itr = basePage.getChildPages().iterator();
                        while (itr.hasNext()) {
                            Page nextPage = itr.next();
                            pageStack.add(nextPage);
            }

                        if (crawlerResponse.getPageDetails() == null) {
                            crawlerResponse.setPageDetails(new ArrayList<Page>());
                        }
                        crawlerResponse.getPageDetails().add(basePage);

                        crawlerResponse.setTotalImages(basePage.getImageCount());
                        crawlerResponse.setTotalLinks(basePage.getChildPages().size());
                        crawlerResponse.setUniqueId(crawlerRequestContract.getUniqueId());

                        while (!pageStack.isEmpty()) {
                            Page page = pageStack.pop();
                            if (page.getCurrLevel() > 0) {
                                page = crawlerLoader.loadPageDetails(page.getPageLink(), page.getCurrLevel(), urlValidator, allLinks);
                                crawlerResponse.getPageDetails().add(page);
                                crawlerResponse.setTotalImages(crawlerResponse.getTotalImages() + page.getImageCount());
                                if (page.getChildPages() != null)
                                    crawlerResponse.setTotalLinks(crawlerResponse.getTotalLinks() + page.getChildPages().size());
                                if (page.getChildPages() != null) {
                                    addChildPagesToStack(pageStack, page, allLinks);
                                }
                            }
                        }
                        System.out.println(crawlerResponse);
                        dataLayer.updateStatus("CrawlerRequest","Processed", crawlerRequestContract.getUniqueId());
                        dataLayer.insertJson(g.toJson(crawlerResponse), crawlerResponse.getClass().getSimpleName());
                    }catch (Exception e){
                        dataLayer.updateStatus("CrawlerRequest","Failed", crawlerRequestContract.getUniqueId());
                    }finally {

                    }
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
    void addChildPagesToStack(Stack<Page> pageStack, Page page, Set<String> allLinks){
        Iterator<Page> itr = page.getChildPages().iterator();
        while(itr.hasNext()) {
            Page childPage = itr.next();
            if(!allLinks.contains(childPage.getPageLink())) {
                pageStack.add(childPage);
            }

        }

    }

}
