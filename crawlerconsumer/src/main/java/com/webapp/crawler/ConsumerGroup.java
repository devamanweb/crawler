package com.webapp.crawler;

import com.webapp.crawler.config.AppConfig;
import com.webapp.crawler.services.CrawlerLoader;

import java.util.ArrayList;
import java.util.List;

public final class ConsumerGroup {

    private final int numberOfConsumers;
    private List<ConsumerThread> consumers;

    public ConsumerGroup(CrawlerLoader crawlerLoader,
                         AppConfig appConfig, DataLayer dataLayer,
                         int numberOfConsumers) {
        this.numberOfConsumers = numberOfConsumers;
        consumers = new ArrayList<>();
        for (int i = 0; i < this.numberOfConsumers; i++) {
            ConsumerThread ncThread =
                    new ConsumerThread(crawlerLoader, appConfig, dataLayer);

            consumers.add(ncThread);
        }
    }

    public void execute() {
        for (ConsumerThread ncThread : consumers) {
            Thread t = new Thread(ncThread);
            t.start();
        }
    }


}