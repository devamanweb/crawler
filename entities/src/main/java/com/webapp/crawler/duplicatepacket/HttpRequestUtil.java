package com.webapp.crawler.duplicatepacket;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class HttpRequestUtil {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    private final Logger logger = LoggerFactory.getLogger(HttpRequestUtil.class);

    public <T> T getInternalApiResponse(URI uri, HttpHeaders headers, Class<T> responseClass) {
        logger.debug("Get Api url : " + uri.toString());
        RestTemplate restTemplate = this.restTemplate;

        ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<T>(headers),
                responseClass);

        return responseEntity.getBody();
    }

    public <T> T getInternalApiResponse(URI uri, HttpHeaders headers, ParameterizedTypeReference<T> responseClass,
                                        boolean useSlowRestTemplate) {
        logger.debug("Get Api url : " + uri.toString());
        RestTemplate restTemplate = this.restTemplate;

        ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers),
                responseClass);
        return responseEntity.getBody();
    }

    public <T> T postJsonRequest(String url, Object data, Class<T> responseClass) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return postJsonRequest(url, data, headers, responseClass);
    }

    public <T> T postJsonRequest(String url, Object data, HttpHeaders headers, Class<T> responseClass) {
        String json = null;
        try {
            json = mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            logger.error("Exception converting object to json", e);
        }
        RestTemplate restTemplate = this.restTemplate;

        @SuppressWarnings({ "rawtypes", "unchecked" })
        HttpEntity request = new HttpEntity(json, headers);

        logger.debug("Post Api url " + url + " Post Data" + json);
        URI uri = URI.create(url.toString());
        return restTemplate.postForObject(uri, request, responseClass);
    }

    public <T> T putJsonRequest(String url, Object data, HttpHeaders headers, Class<T> type,
                                boolean useSlowRestTemplate) {
        String json = null;
        try {
            json = mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            logger.error("Exception converting object to json", e);
            throw new RuntimeException(e);
        }
        @SuppressWarnings({ "rawtypes", "unchecked" })
        HttpEntity request = new HttpEntity(json, headers);
        RestTemplate restTemplate = this.restTemplate;

        logger.debug("Put Api url " + url + " Put Data" + json);
        ResponseEntity<T> apiResponse = restTemplate.exchange(url, HttpMethod.PUT, request, type);
        if (apiResponse == null) {
            logger.error(new StringBuilder("PUT Request failed for url ").append(url).toString());
        }
        return apiResponse.getBody();
    }
}