package com.webapp.crawler.duplicatepacket;

public class DuplicatePacketResponse {

	private Boolean isRequestDuplicate;
	private String generatedHashCode;

	public Boolean getIsRequestDuplicate() {
		return isRequestDuplicate;
	}

	public void setIsRequestDuplicate(Boolean isRequestDuplicate) {
		this.isRequestDuplicate = isRequestDuplicate;
	}

	public String getGeneratedHashCode() {
		return generatedHashCode;
	}

	public void setGeneratedHashCode(String generatedHashCode) {
		this.generatedHashCode = generatedHashCode;
	}

}
