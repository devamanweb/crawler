package com.webapp.crawler.duplicatepacket;

import com.webapp.crawler.datalayer.constants.Constants;
import com.webapp.crawler.entities.containers.RequestContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DuplicatePacketAdapter {


	private static final Logger LOGGER = LoggerFactory.getLogger(DuplicatePacketAdapter.class);

	@Autowired
	private HttpRequestUtil httpUtil;

	public DuplicatePacketResponse checkForDuplicatePacket(RequestContract request) {
		long beforeInvokeMilis = System.currentTimeMillis();

		DuplicatePacketResponse duplicatePacketResponse = null;
		long startTime = System.currentTimeMillis();
		try {
			duplicatePacketResponse = httpUtil.postJsonRequest(getRedisUrl(request), request,
					DuplicatePacketResponse.class);
		} catch (Exception ex) {
			duplicatePacketResponse = new DuplicatePacketResponse();
			duplicatePacketResponse.setIsRequestDuplicate(false);
			LOGGER.info(
					"{} - Exception occured while fetching the response from Duplicate packet API. Hence accepting the request!!",
					request.getUniqueId());
			ex.printStackTrace();
		}
		finally {

		}
		long endTime = startTime - System.currentTimeMillis();
		LOGGER.info("Validation Request Processed in millies : {}", endTime);
		return duplicatePacketResponse;
	}

	private String getRedisUrl(RequestContract request) {
		return Constants.REDIS_URL + Constants.REDIS_CRAWLER + Constants.FORWARD_SLASH;
	}

}
