package com.webapp.crawler.datalayer.constants;

public interface Constants {

	String REDIS_URL = "http://RedisService/v1/checkDuplicatePacket/";

	String REDIS_CRAWLER = "crawler";
	
	String FORWARD_SLASH = "/";

}
