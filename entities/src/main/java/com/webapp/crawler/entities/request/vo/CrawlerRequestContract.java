package com.webapp.crawler.entities.request.vo;


import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.webapp.crawler.entities.containers.RequestContract;

@JsonTypeName("CrawlerRequestContract")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "fieldType", defaultImpl = CrawlerRequestContract.class)

public class CrawlerRequestContract extends RequestContract{

    protected String url;
    protected int depth;
    protected String status;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "  source=" + source +
                ", status=" + status +
                ", uniqueId='" + uniqueId + '\'' +
                ", isValidated=" + isValidated +
                ", maxRetries=" + maxRetries +
                ", generatedHashCode='" + generatedHashCode + '\'' +
                ", transactionid='" + transactionid + '\'' +
                ",url='" + url + '\'' +
                ", depth=" + depth +
                '}';
    }
}
