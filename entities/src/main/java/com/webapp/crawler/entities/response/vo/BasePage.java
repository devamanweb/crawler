package com.webapp.crawler.entities.response.vo;


import java.util.Set;

public class BasePage {
    Set<String> links;

    public Set<String> getLinks() {
        return links;
    }

    public void setLinks(Set<String> links) {
        this.links = links;
    }
}
