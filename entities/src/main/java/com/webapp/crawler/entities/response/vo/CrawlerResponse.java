package com.webapp.crawler.entities.response.vo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)

public class CrawlerResponse {
    int totalLinks;
    int totalImages;
    List<Page> pageDetails;
    String uniqueId;

    public int getTotalLinks() {
        return totalLinks;
    }

    public void setTotalLinks(int totalLinks) {
        this.totalLinks = totalLinks;
    }

    public int getTotalImages() {
        return totalImages;
    }

    public void setTotalImages(int totalImages) {
        this.totalImages = totalImages;
    }

    public List<Page> getPageDetails() {
        return pageDetails;
    }

    public void setPageDetails(List<Page> pageDetails) {
        this.pageDetails = pageDetails;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public String toString() {
        return "{" +
                "totalLinks=" + totalLinks +
                ",uniqueId=" + uniqueId +
                ", totalImages=" + totalImages +
                ", pageDetails=[" + pageDetails +
                "]}";
    }
}
