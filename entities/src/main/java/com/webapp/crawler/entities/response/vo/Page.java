package com.webapp.crawler.entities.response.vo;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

@Component
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "fieldType", defaultImpl = Page.class)
public class Page {

    String pageTitle;
    String pageLink;
    int imageCount;
    Set<Page> childPages;
    int currLevel;

    @Override
    public String toString() {
        return "Page{" +
                "pageTitle='" + pageTitle + '\'' +
                ", pageLink='" + pageLink + '\'' +
                ", imageCount=" + imageCount +
                ", childPages=" + childPages +
                ", currLevel=" + currLevel +
                '}';
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public Set<Page> getChildPages() {
        return childPages;
    }

    public void setChildPages(Set<Page> childPages) {
        this.childPages = childPages;
    }

    public int getCurrLevel() {
        return currLevel;
    }

    public void setCurrLevel(int currLevel) {
        this.currLevel = currLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return pageLink.equals(page.pageLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageLink);
    }
}
