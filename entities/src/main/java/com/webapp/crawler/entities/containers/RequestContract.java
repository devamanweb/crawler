package com.webapp.crawler.entities.containers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;

@JsonIgnoreProperties(ignoreUnknown = true)

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT, property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = CrawlerRequestContract.class, name = "CrawlerRequestContract")
})
public  class RequestContract  {

	protected int source;
	protected String uniqueId;
	protected boolean isValidated;
	protected int maxRetries;
	protected String generatedHashCode;
	protected  String transactionid;


	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}


	public boolean isValidated() {
		return isValidated;
	}

	public void setValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}

	public int getMaxRetries() {
		return maxRetries;
	}

	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}


	public String getGeneratedHashCode() {
		return generatedHashCode;
	}

	public void setGeneratedHashCode(String generatedHashCode) {
		this.generatedHashCode = generatedHashCode;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	@Override
	public String toString() {
		return "{" +
				" source=" + source +
				", uniqueId='" + uniqueId + '\'' +
				", isValidated=" + isValidated +
				", maxRetries=" + maxRetries +
				", generatedHashCode='" + generatedHashCode + '\'' +
				", transactionid='" + transactionid + '\'' +
				'}';
	}
}
