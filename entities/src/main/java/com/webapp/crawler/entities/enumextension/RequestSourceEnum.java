package com.webapp.crawler.entities.enumextension;

import java.util.HashMap;
import java.util.Map;

public enum RequestSourceEnum {
	CRAWL(0), SPYDER(1), BSP(2) , MOSPYDER(3);

	private RequestSourceEnum(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	private int code;
	private static Map<Integer, String> map = new HashMap<>();

	static {
		for (RequestSourceEnum requestSourceEnum : RequestSourceEnum.values()) {
			map.put(requestSourceEnum.code, requestSourceEnum.toString());
		}
	}

	public static String valueOf(int code) {
		return map.get(code);
	}

}
