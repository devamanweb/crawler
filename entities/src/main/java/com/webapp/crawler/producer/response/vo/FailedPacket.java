package com.webapp.crawler.producer.response.vo;

public class FailedPacket {

	private String uniqueId;
	private String errorCode;
	private String errorDescription;

	public String getErrorDescription() {
		return errorDescription;
	}

	public FailedPacket setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
		return this;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public FailedPacket setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
		return this;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public FailedPacket setErrorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}

}
