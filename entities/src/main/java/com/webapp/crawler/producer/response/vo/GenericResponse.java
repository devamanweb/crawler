package com.webapp.crawler.producer.response.vo;

public class GenericResponse extends ResponseContract {

	String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
