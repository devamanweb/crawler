package com.webapp.crawler.util.hashcodegenerator.requestcontract.impl;

import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.entities.request.vo.CrawlerRequestContract;
import com.webapp.crawler.util.hashcodegenerator.requestcontract.RequestContractHashCodeGenerator;
import org.springframework.stereotype.Component;

@Component("CrawlerRequestHashCodeGenerator")
public class CrawlerRequestHashCodeGenerator implements RequestContractHashCodeGenerator {

	@Override
	public String generateHashCode(RequestContract request) {
		CrawlerRequestContract crawlerRequestContract = (CrawlerRequestContract) request;
		return crawlerRequestContract.getUrl() + crawlerRequestContract.getDepth();
	}

}
