package com.webapp.crawler.util;

import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.entities.enumextension.RequestSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SequenceGeneratorAdaptor {

	@Autowired
	SequenceGenerator sequenceGenerator;

	public void setRequestUniqueId(RequestContract request) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(sequenceGenerator.nextId());
		request.setUniqueId(RequestSourceEnum.valueOf(request.getSource()) + stringBuilder.toString());
	}

}
