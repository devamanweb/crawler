package com.webapp.crawler.util.hashcodegenerator.requestcontract;

import com.webapp.crawler.entities.containers.RequestContract;

public interface RequestContractHashCodeGenerator {

	String generateHashCode(RequestContract request);

}