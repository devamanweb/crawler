package com.webapp.crawler.util.hashcodegenerator;

import com.webapp.crawler.entities.containers.RequestContract;
import com.webapp.crawler.util.hashcodegenerator.requestcontract.RequestContractHashCodeGenerator;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericHashCodeGenerator {

	@Autowired
	RequestContractHashCodeGenerator requestContractHashCodeGenerator;

	private static final Logger LOGGER = LoggerFactory.getLogger(GenericHashCodeGenerator.class);

	public String generateHashCode(String request) {
		return DigestUtils.sha256Hex(request);
	}

	public String generateHashCode(RequestContract request) {
		String key = "";
		try {
			key = requestContractHashCodeGenerator.generateHashCode(request);
			key = generateHashCode(key);
			LOGGER.debug("{} - Following key generated :{}", request.getUniqueId(), key);
		} catch (Exception ex) {
			LOGGER.info("{} - Following exception occured while generating key :{}", request.getUniqueId(),
					ex.fillInStackTrace());
		}
		return key;
	}


}
